Application is a standard Java project importable in Eclipse.

In order to launch the BroadcastRecoveryTest of the application, a JVM parameter must be specified:

-Dapplication.home=/path/to/application

This path is read through the deployment file: resources/GCMA.xml