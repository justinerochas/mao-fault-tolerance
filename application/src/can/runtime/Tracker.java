package can.runtime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.objectweb.proactive.ActiveObjectCreationException;
import org.objectweb.proactive.Body;
import org.objectweb.proactive.RunActive;
import org.objectweb.proactive.annotation.multiactivity.DefineGroups;
import org.objectweb.proactive.annotation.multiactivity.Group;
import org.objectweb.proactive.annotation.multiactivity.MemberOf;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.api.PAFuture;
import org.objectweb.proactive.core.node.Node;
import org.objectweb.proactive.core.node.NodeException;
import org.objectweb.proactive.core.util.wrapper.LongWrapper;
import org.objectweb.proactive.multiactivity.MultiActiveService;

import can.architecture.Peer;
import can.architecture.RoutingPair;
import can.data.BalancedDataBuilder;
import can.data.Key;
import can.data.Value;
import can.messages.AddQuery;
import can.messages.AddResponse;
import can.policy.EqualSizeNetworkBuilder;
import can.policy.NetworkBuilder;
import can.tests.ftbroadcast.BroadcastRecoveryTest;

@DefineGroups({
	@Group(name="executeAndKill", selfCompatible=true),
})
public class Tracker implements Serializable, RunActive {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(Tracker.class.getName());
	private static final String logTag = "[" + Tracker.class.getSimpleName() + "] ";

	// Network matter
	private NetworkBuilder networkBuilder;	
	private ArrayList<Node> nodes;

	// Data matter
	private List<Key> keysList;
	private List<Value> valuesList;
	private int dataAddIteration;
	
	// Broadcast matter
	private int broadcastReceptionCounter;

	@Override
	public void runActivity(Body body) {
		MultiActiveService service = new MultiActiveService(body);
		while (body.isActive()) {
			service.multiActiveServing();
		}
	}

	public Tracker() {
		this.networkBuilder = new EqualSizeNetworkBuilder();
		this.keysList = BalancedDataBuilder.buildKeys();
		this.valuesList = BalancedDataBuilder.buildValues();
		this.dataAddIteration = 0;
		this.broadcastReceptionCounter = 0;
	}

	public Tracker(ArrayList<Node> nodes) {
		this();
		this.nodes = nodes;
	}

	@MemberOf("executeAndKill")
	public NetworkBuilder getNetworkBuilder() {
		return this.networkBuilder;
	}

	public void createNetwork() {
		log.info(logTag + "Creating network.");
		Peer p = null;
		try {
			p = PAActiveObject.newActive(Peer.class, new Object[] {new LongWrapper(0), 
					PAActiveObject.getStubOnThis()}, this.nodes.get(0));
		} 
		catch (ActiveObjectCreationException | NodeException e) {
			e.printStackTrace();
		}
		PAFuture.waitFor(p.join(null, 0));
		this.networkBuilder.add(p);	
		Peer joinP;
		for (int pID = 1 ; pID < Constants.NB_PEERS ; pID++) {
			try {
				p = PAActiveObject.newActive(Peer.class, new Object[] {new LongWrapper(pID), 
						PAActiveObject.getStubOnThis()}, nodes.get(pID));
			} 
			catch (ActiveObjectCreationException | NodeException e) {
				e.printStackTrace();
			}
			joinP = this.networkBuilder.getPeerToJoin();
			// Need to synchronize here to be sure the network is well formed 
			// before adding a new peer 
			PAFuture.waitFor(p.join(joinP, this.networkBuilder.getSplitDimension()));
			System.out.println("Peer " + p.asynchronousToString() + " added in the network.");
			this.networkBuilder.add(p);
		}
		log.info(logTag + "Network created.");
	}

	@MemberOf("executeAndKill")
	public void addAllData(BroadcastRecoveryTest coordinator) {	
		if (this.dataAddIteration == 0) {
			log.info(logTag + "Adding data - number: " + keysList.size());
		}
		this.addData();
		this.dataAddIteration++;
		if (this.dataAddIteration < this.keysList.size()) {
			((Tracker) PAActiveObject.getStubOnThis()).addAllData(coordinator);
		} 
		else {
			log.info(logTag + this.networkBuilder);
			this.computeAllDataSum(false);
			((Tracker) PAActiveObject.getStubOnThis()).broadcast(coordinator);
			log.info(logTag + "Data added.");
		}
	}

	public void addData() {
		Peer departurePeer = this.networkBuilder.getRandomPeer();
		AddResponse response = departurePeer.add(new AddQuery(
				keysList.get(this.dataAddIteration), valuesList.get(this.dataAddIteration)));
		PAFuture.waitFor(response);
	}
	
	@MemberOf("executeAndKill")
	public void broadcast(BroadcastRecoveryTest coordinator) {
		log.info(logTag + "Starting broadcast.");
		Peer departurePeer;
		departurePeer = this.networkBuilder.getPeerWithId(3);
		log.info(logTag + "Broadcast seed is Peer#" + departurePeer.getIdentifier().getLongValue());
		int[] spatialConstraintPoint = new int[Constants.NB_DIMENSIONS];
		for (int i = 0 ; i < Constants.NB_DIMENSIONS ; i++) {
			spatialConstraintPoint[i] = departurePeer.getManagedBoundOnDimension(i).min;
		}
		Key spatialConstraintKey = new Key(spatialConstraintPoint);
		RoutingPair initialRoutingPair = new RoutingPair(Constants.NB_DIMENSIONS, true);
		departurePeer.broadcast(spatialConstraintKey, initialRoutingPair, 0, coordinator);
		log.info(logTag + "Broadcast started.");
	}
	
	public void notifyReception(BroadcastRecoveryTest coordinator) {
		this.broadcastReceptionCounter++;
		if (this.broadcastReceptionCounter == this.networkBuilder.getNumberOfPeers()) {
			log.info(logTag + this.networkBuilder);
			this.computeAllDataSum(true);
			coordinator.checkCorrectBroadcastReexecution(this.networkBuilder.getPeers());
		}
	}
	
	private void computeAllDataSum(boolean afterRecovery) {
		log.info(logTag + "Computing sum of values.");	
		Peer p;
		Map<Key, Value> store;
		long sum = 0;
		Iterator<Peer> i = this.networkBuilder.getPeers().iterator();
		while (i.hasNext()) {
			p = i.next();
			store = p.getDataManager().getStoredValues();
			for (Entry<Key, Value> entry: store.entrySet()) {
				sum += entry.getValue().getInternalValue().getIntValue();
			}
		}
		log.info(logTag + "Sum of values computed (" + sum + ").");
	}

}

