package can.runtime;

import java.util.Random;

public class Constants {

	public static int NB_DIMENSIONS = 2;

	public static final int NB_PEERS = 5;
	
	public static final int NB_TRACKERS = 1;

	public static final double SPLIT_COEFFICIENT = 0.5;

	public static final int CAN_BOUND_MIN = 0;

	public static final int CAN_BOUND_MAX = 100;

	public static final Random random = new Random();

	// Fault tolerance 
	public static final int NB_RECOVERED_PEERS = 1;

	public static final String FT_PROTOCOL_PROPERTY = "-proto";

	public static final String FT_PROTOCOL = "cic" ;

	public static final String FT_SERVER_CLASSNAME = 
			"org.objectweb.proactive.core.body.ft.servers.StartFTServer";

	// GCM Constants
	public static final String GCMA_LOCATION = "/GCMA.xml";

	public static final String PEER_VN_NAME = "peers";

	public static final int NB_NODES = NB_TRACKERS + NB_PEERS + NB_RECOVERED_PEERS ;
	
	// Experiments
	public static final String EXPERIMENT_FOLDER_PATH = "/local/home/jrochas/projects/mao-fault-tolerance/application/experiments";
	
	public static final String EXPERIMENT_STATISTICS_FOLDER_NAME = "statistics" ;
	
	public static final String EXPERIMENT_FILE_TAG = "run-" ;

}
