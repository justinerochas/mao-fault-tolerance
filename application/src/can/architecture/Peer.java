package can.architecture;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.objectweb.proactive.Body;
import org.objectweb.proactive.RunActive;
import org.objectweb.proactive.annotation.multiactivity.Compatible;
import org.objectweb.proactive.annotation.multiactivity.DefineGroups;
import org.objectweb.proactive.annotation.multiactivity.DefinePriorities;
import org.objectweb.proactive.annotation.multiactivity.DefineRules;
import org.objectweb.proactive.annotation.multiactivity.DefineThreadConfig;
import org.objectweb.proactive.annotation.multiactivity.Group;
import org.objectweb.proactive.annotation.multiactivity.MemberOf;
import org.objectweb.proactive.annotation.multiactivity.PriorityHierarchy;
import org.objectweb.proactive.annotation.multiactivity.PrioritySet;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.api.PAFuture;
import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;
import org.objectweb.proactive.core.util.wrapper.LongWrapper;
import org.objectweb.proactive.core.util.wrapper.StringWrapper;
import org.objectweb.proactive.multiactivity.MultiActiveService;

import can.data.EmptyValue;
import can.data.Key;
import can.data.Value;
import can.messages.AddQuery;
import can.messages.AddResponse;
import can.messages.LookupQuery;
import can.messages.LookupResponse;
import can.runtime.Constants;
import can.runtime.Tracker;
import can.runtime.Utils;
import can.tests.ftbroadcast.BroadcastRecoveryTest;

@DefineGroups({
	@Group(name="gettersOnImmutable", selfCompatible=true),
	@Group(name="dataManagement", selfCompatible=true),
	@Group(name="broadcasting", selfCompatible=false),
	@Group(name="monitoring", selfCompatible=true),
})
@DefineRules({
	@Compatible({"gettersOnImmutable", "broadcasting", "dataManagement"}),
	@Compatible({"gettersOnImmutable", "monitoring"})
})
@DefinePriorities({
	@PriorityHierarchy({
		@PrioritySet({"gettersOnImmutable"}),
		@PrioritySet({"broadcasting", "dataManagement"}),
		@PrioritySet({"monitoring"})
	})
})
@DefineThreadConfig(threadPoolSize=2, hardLimit=false)
public class Peer implements Serializable, RunActive { 

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(Peer.class.getName());
	private static final String logTag = "[" + Peer.class.getSimpleName() + "] ";

	// CAN matter
	private Tracker tracker;
	private LongWrapper identifier;
	private Zone zone;
	private RouteManager routeManager;
	private DataManager dataManager;

	// Broadcast matter
	private int broadcastReceptionChecker;
	private boolean broadcastAlreadyReceived;

	@Override
	public void runActivity(Body body) {
		MultiActiveService service = new MultiActiveService(body);
		while (body.isActive()) {
			service.multiActiveServing();
		}
	}
	
	public Peer() {
		this.broadcastReceptionChecker = 0;
		this.broadcastAlreadyReceived = false;
	}

	public Peer(LongWrapper id, Tracker tracker) {
		this();
		this.identifier = id;
		this.tracker = tracker;
	}

	@MemberOf("gettersOnImmutable")
	public LongWrapper getIdentifier() {
		return this.identifier;
	} 

	@MemberOf("gettersOnImmutable")
	public Zone getZone() {
		return this.zone;
	}
	
	public RouteManager getRouteManager() {
		return this.routeManager;
	}

	public DataManager getDataManager() {
		return this.dataManager;
	}
	
	public int getBroadcastReceptionChecker() {
		return this.broadcastReceptionChecker;
	}
	
	public boolean getBroadcastAlreadyReceived() {
		return this.broadcastAlreadyReceived;
	}

	public BooleanWrapper join(Peer p, int dimension) {
		BooleanWrapper couldJoin = new BooleanWrapper(true);
		this.routeManager = new RouteManager(
				(Peer) PAActiveObject.getStubOnThis(), this.identifier);
		this.dataManager = new DataManager();
		// Bootstrap
		if (p == null) {
			this.zone = new Zone((Peer) PAActiveObject.getStubOnThis());
			couldJoin = new BooleanWrapper(true);
		}
		// Normal case
		else {
			this.zone = new Zone((Peer) PAActiveObject.getStubOnThis(), 
					p.getZone(), dimension);
			LongWrapper pIdentifier = p.getIdentifier();
			PAFuture.waitFor(pIdentifier);
			PAFuture.waitFor(p.splitZone(dimension));
			// ProActive bug: if the splitZone call is not waited, the following 
			// wait-by-necessity does not work
			if (couldJoin.getBooleanValue()) {
				this.routeManager.updateNeighboringWith(pIdentifier, p, dimension, this.zone);
			}
			else {
				System.err.println("Peer " + this.identifier + " could not join"
						+ " because zone to split of peer " + p.getIdentifier()
						.getLongValue() + " is too small.");
			}
		}
		return couldJoin;
	}

	public BooleanWrapper splitZone(int dimension) {
		return this.zone.divideMaxPerTwo(dimension);
	}

	public BooleanWrapper manages(Key key) {
		if (this.zone.contains(key)) {
			return new BooleanWrapper(true);
		}
		return new BooleanWrapper(false);
	}

	@MemberOf("gettersOnImmutable")
	public Bound getManagedBoundOnDimension(int dimension) {
		return this.zone.getBoundOnDimension(dimension);
	}

	public BooleanWrapper addNeighbor(LongWrapper neighborID, Peer neighbor, int dimension) {
		return this.routeManager.addNeighbor(neighborID, neighbor, dimension);
	}

	public BooleanWrapper removeNeighbor(LongWrapper neighborID, Peer neighbor, int dimension) {
		return this.routeManager.removeNeighbor(neighborID , neighbor, dimension);
	}

	@MemberOf("dataManagement")
	public AddResponse add(AddQuery query) {
		AddResponse response;
		if (this.manages(query.getKey()).getBooleanValue()) {
			this.dataManager.store(query);
			response = new AddResponse(this.identifier);
		}
		else {
			query.addtraversedPeerID(this.identifier);
			response = this.routeManager.getPeerClosestTo(this.zone, query).add(query);
			response.addtraversedPeerID(this.identifier);
		}
		return response;
	}

	public LookupResponse lookup(LookupQuery query) {
		LookupResponse response;
		if (this.manages(query.getKey()).getBooleanValue()) {
			Value lookupValue;
			if (this.dataManager.containsKey(query.getKey())) {
				lookupValue = this.dataManager.getValue(query.getKey());
			}
			else {
				lookupValue = new EmptyValue();
			}
			response = new LookupResponse(lookupValue, this.identifier);
		}
		else {
			query.addtraversedPeerID(this.identifier);
			response = this.routeManager.getPeerClosestTo(this.zone, query).lookup(query);
			response.addtraversedPeerID(this.identifier);
		}
		return response;
	}

	@MemberOf("broadcasting")
	public void broadcast(Key spatialConstraint, RoutingPair routingPair, int broadcastIteration, BroadcastRecoveryTest coordinator) {
		log.info(logTag + "Peer#" + this.identifier + " received the broadcast.");
		if (broadcastIteration == 2) {
			coordinator.kill();
		}
		Utils.sleep(4000);
		if (this.broadcastReceptionChecker == 1) {
			broadcastAlreadyReceived = true;
		}
		else {
			this.broadcastReceptionChecker = 1;
			for (Entry<Key,Value> entry: this.dataManager.getStoredValues().entrySet()) {
				entry.getValue().addOne();
			}
		}
		this.tracker.notifyReception(coordinator);
		// Now determine to which neighbors it is needed to forward
		int receptionDimension = routingPair.receptionDimension;
		boolean ascending = routingPair.ascending;
		boolean willBeAscending = false;
		Peer peerOnDim;
		Map<LongWrapper, Peer> peersOnDim;
		Peer candidateNeighbor;
		Map<Peer, RoutingPair> candidateNeighbors = new HashMap<Peer, RoutingPair>();
		Map<Peer, RoutingPair> neighborsToSend = new HashMap<Peer, RoutingPair>();
		// Neighbors that are along dimensions 0 ... k-1
		for (int i = 0 ; i < receptionDimension ; i++) {
			peersOnDim = this.routeManager.getNeighborsCopyOnDimension(i);
			for (Entry<LongWrapper, Peer> entry: peersOnDim.entrySet()) {
				peerOnDim = entry.getValue();
				// The neighbor has his minimum bound greater than the peer own maximum bound ==> ascending direction
				Bound peerBounds = peerOnDim.getManagedBoundOnDimension(i);
				PAFuture.waitFor(peerBounds);
				if (peerBounds.min >= this.getManagedBoundOnDimension(i).max) {
					willBeAscending = true;
				}
				else{
					willBeAscending = false;
				}
				candidateNeighbors.put(peerOnDim, new RoutingPair(i, willBeAscending));
			}
		}
		// Neighbors on dimension k in the same direction
		peersOnDim = this.routeManager.getNeighborsCopyOnDimension(receptionDimension);
		for (Entry<LongWrapper, Peer> entry: peersOnDim.entrySet()) {
			peerOnDim = entry.getValue();
			// The peer is located on the same direction as sending direction
			Bound peerBound = peerOnDim.getManagedBoundOnDimension(receptionDimension);
			PAFuture.waitFor(peerBound);
			int minimumBound = peerBound.min;
			if (minimumBound >= this.getManagedBoundOnDimension(receptionDimension).max && ascending) {
				// Either in ascending direction
				willBeAscending = true;
				candidateNeighbors.put(peerOnDim, new RoutingPair(receptionDimension, willBeAscending));
			}
			else {
				// Or in descending direction
				if (peerOnDim.getManagedBoundOnDimension(receptionDimension).max <= this.getManagedBoundOnDimension(receptionDimension).min && !ascending) {
					willBeAscending = false;
					candidateNeighbors.put(peerOnDim, new RoutingPair(receptionDimension, willBeAscending));
				}
			}
		}
		// Apply the constraints
		RoutingPair candidateRoutingPair;
		boolean sendToCandidateNeighbor;
		for (Entry<Peer, RoutingPair> entry: candidateNeighbors.entrySet()) {
			candidateNeighbor = entry.getKey();
			candidateRoutingPair = entry.getValue();
			sendToCandidateNeighbor = true;
			// Check for that peer the dimensions 1 ... futureReceptionDimension - 1 against the spatial constraint
			if (!candidateNeighbor.getZone().containsHyperplaneUpToDim(spatialConstraint, candidateRoutingPair.receptionDimension-1)) {
				sendToCandidateNeighbor = false;
			}
			else {
				// Check for that peer the dimensions receptionDimension + 1 ... D against the corner criteria
				for (int i = candidateRoutingPair.receptionDimension + 1 ; i < Constants.NB_DIMENSIONS ; i++) {
					Bound candidateNeighborBound = candidateNeighbor.getManagedBoundOnDimension(i);
					Bound peerBound = this.getManagedBoundOnDimension(i);
					// The peer touching the lowest corner is responsible to send
					if (!(peerBound.min <= candidateNeighborBound.min && candidateNeighborBound.min < peerBound.max)) {
						sendToCandidateNeighbor = false;
						break;
					}
				}
				if (sendToCandidateNeighbor) {
					neighborsToSend.put(candidateNeighbor, candidateRoutingPair);
				}
			}
		}
		// Send to neighbors satisfying the constraints
		for (Entry<Peer, RoutingPair> entry: neighborsToSend.entrySet()) {
			log.info(logTag + "Peer#" + this.identifier + " forwarded the broadcast to Peer#" + entry.getKey().getIdentifier() + ".");
			entry.getKey().broadcast(spatialConstraint, entry.getValue(), broadcastIteration+1, coordinator);
		}
	}

	/**
	 * Warning: cannot be called asynchronously
	 */
	@Override
	public String toString() {
		return "Peer#" + this.identifier + " broadcastValue=" + this.broadcastReceptionChecker + " alreadyReceived=" + this.broadcastAlreadyReceived 
				+ " zone="+ this.zone.toString() + " neighbors=(" + this.routeManager.toString() + ")";
	}

	@MemberOf("monitoring")
	public StringWrapper asynchronousToString() {
		return new StringWrapper(this.toString());
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Peer) {
			return this.identifier.equals(((Peer) o).identifier);
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.identifier.hashCode();
	}

}
