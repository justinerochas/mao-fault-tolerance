package can.architecture;

import java.io.Serializable;

public class RoutingPair implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public final int receptionDimension;
	public final boolean ascending;
	
	public RoutingPair(int receptionDimension, boolean ascending) {
		this.receptionDimension = receptionDimension;
		this.ascending = ascending;
	}

}
