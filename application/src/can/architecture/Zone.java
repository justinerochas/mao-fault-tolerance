package can.architecture;

import java.io.Serializable;
import java.util.ArrayList;

import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;

import can.data.Key;
import can.runtime.Constants;

public class Zone implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<Bound> bounds;
	
	private Peer owner;
	
	/**
	 * Used for bootstrap of the network.
	 * @param owner The peer that owns the zone.
	 */
	public Zone(Peer owner) {
		this.owner = owner;
		this.bounds = new ArrayList<Bound>(Constants.NB_DIMENSIONS);
		for (int i = 0 ; i < Constants.NB_DIMENSIONS ; i++) {
			this.bounds.add(new Bound(Constants.CAN_BOUND_MIN, Constants.CAN_BOUND_MAX));
		}
	}
	
	/**
	 * Usual way to create a zone by splitting another zone.
	 * @param owner The peer that owns the zone.
	 * @param zone The zone (that already belongs to the network) to split.
	 * @param dimension The dimension on which to split. The two zones will 
	 * share a border on that dimension.
	 */
	public Zone(Peer owner, Zone zone, int dimension) {
		this.owner = owner;
		this.bounds = new ArrayList<Bound>(Constants.NB_DIMENSIONS);
		for (int i = 0 ; i < Constants.NB_DIMENSIONS ; i++) {
			if (i == dimension) {
				this.bounds.add(
						new Bound((int) (zone.bounds.get(i).min 
								+ (zone.bounds.get(i).max - zone.bounds.get(i).min) 
								* Constants.SPLIT_COEFFICIENT), zone.bounds.get(i).max));
			}
			else {
				this.bounds.add(
						new Bound(zone.bounds.get(i).min, 
								zone.bounds.get(i).max));
			}
		}
	}
	
	public Peer getOwner() {
		return this.owner;
	}
	
	public Bound getBoundOnDimension(int dimension) {
		return this.bounds.get(dimension);
	}
	
	public BooleanWrapper divideMaxPerTwo(int dimension) {
		int newInterval = (int) ((this.bounds.get(dimension).max 
				- this.bounds.get(dimension).min) * Constants.SPLIT_COEFFICIENT);
		if (newInterval == 0) {
			System.err.println("Zone is too small for a join, "
					+ "cannot separate it into two pieces.");
			return new BooleanWrapper(false);
		}
		else {
			this.bounds.set(dimension, new Bound(this.bounds.get(dimension).min, 
					this.bounds.get(dimension).min + newInterval));
			return new BooleanWrapper(true);
		}
	}

	public boolean touch(Zone z, int dimension) {
		int thisMin, thisMax, zMin, zMax;
		for (int i = 0 ; i < Constants.NB_DIMENSIONS ; i++) {	
			thisMin = this.bounds.get(i).min;
			thisMax = this.bounds.get(i).max;
			zMin = z.bounds.get(i).min;
			zMax = z.bounds.get(i).max;
			// On the considered dimension, the bounds should touch each other
			if (i == dimension) {
				if (!(thisMin == zMax || thisMax == zMin)) {
					return false; 
				}
			}
			// On the other dimensions, two zones that touch each other either 
			// have the min or the max included in the other bound or the other
			// bound is included in the min and the max
			else {
				if (!((thisMin >= zMin && thisMin < zMax)
						|| (thisMax > zMin && thisMax <= zMax)
						|| (zMin >= thisMin && zMin < thisMax && zMax > thisMin && zMax <= thisMax))) {
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean contains(Key value) {
		for (int i = 0 ; i < Constants.NB_DIMENSIONS ; i++) {
			if (value.get(i) < this.bounds.get(i).min ||
					value.get(i) >= this.bounds.get(i).max) {
				return false;
			}
		}
		return true;
	}
	
	public boolean containsHyperplaneUpToDim(Key value, int highestDim) {
		for (int i = 0 ; i < highestDim ; i++) {
			if (value.get(i) < this.bounds.get(i).min ||
					value.get(i) >= this.bounds.get(i).max) {
				return false;
			}
		}
		return true;
	}
	
	public boolean containsOnDimension(Key value, int dimension) {
		if (value.get(dimension) >= this.bounds.get(dimension).min && 
				value.get(dimension) < this.bounds.get(dimension).max) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (Bound bound: this.bounds) {
			b.append("[" + bound.min + " " + bound.max + "]");
		}
		return b.toString();
	}

}
