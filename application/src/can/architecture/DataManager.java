package can.architecture;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import can.data.Key;
import can.data.Value;
import can.messages.AddQuery;

public class DataManager implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Map<Key, Value> storedValues;

	public DataManager() {
		this.storedValues = new HashMap<Key, Value>();
	}
	
	public void store(AddQuery query) {
		if (this.storedValues.containsKey(query.getKey())) {
			System.err.println("The value of " + query.getKey() + " has been overriden.");
		}
		this.storedValues.put(query.getKey(), query.getValue());
	}

	public boolean containsKey(Key key) {
		return this.storedValues.containsKey(key);
	}
	
	public Value getValue(Key key) {
		return this.storedValues.get(key);
	}
	
	public int getStoredValuesSize() {
		return this.storedValues.size();
	}
	
	public Map<Key, Value> getStoredValues() {
		return this.storedValues;
	}

}
