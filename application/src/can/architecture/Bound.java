package can.architecture;

import java.io.Serializable;

public class Bound implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int min;	
	public int max;
	
	
	public Bound(int min, int max) {
		this.min = min;
		this.max = max;
	}
	
	/**
	 * Minimum value is included whereas maximum value is not.
	 */
	public boolean contains(int value) {
		if (value >= this.min && value < this.max) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
