package can.architecture;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.objectweb.proactive.api.PAFuture;
import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;
import org.objectweb.proactive.core.util.wrapper.LongWrapper;

import can.data.Key;
import can.messages.Query;
import can.runtime.Constants;

public class RouteManager implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Peer owner;

	private final LongWrapper ownerID;

	private Map<Integer, Map<LongWrapper, Peer>> neighbors;

	public RouteManager(Peer owner, LongWrapper ownerID) {
		this.owner = owner;
		this.ownerID = ownerID;
		this.neighbors = new HashMap<Integer, Map<LongWrapper, Peer>>();	
	}

	public BooleanWrapper addNeighbor(LongWrapper pID, Peer p, int dimension) {
		if (this.neighbors.get(dimension) == null) {
			this.neighbors.put(dimension, new HashMap<LongWrapper, Peer>());
		}
		this.neighbors.get(dimension).put(pID, p);
		return new BooleanWrapper(true);
	}

	public BooleanWrapper removeNeighbor(LongWrapper pID, Peer p, int dimension) {
		this.neighbors.get(dimension).remove(pID);
		return new BooleanWrapper(true);
	}

	public void updateNeighboringWith(LongWrapper pIdentifier, Peer p, int dimension, Zone zone) {
		// Update all neighbors of p
		for (Entry<Integer, Map<LongWrapper, Peer>> dimensionEntry : p.getRouteManager().neighbors.entrySet()) {
			Map<LongWrapper, Peer> tmpSet = new HashMap<LongWrapper, Peer>(dimensionEntry.getValue());
			for (Entry<LongWrapper, Peer> peerEntry : tmpSet.entrySet()) {
				Peer neighbor = peerEntry.getValue();
				// If the neighbor is not a neighbor of p any more
				if (! neighbor.getZone().touch(p.getZone(), dimensionEntry.getKey())) {
					// Update p neighbors
					PAFuture.waitFor(p.removeNeighbor(peerEntry.getKey(), neighbor, dimensionEntry.getKey()));
					// Update neighbor neighbors, Peer neighbor
					PAFuture.waitFor(neighbor.removeNeighbor(pIdentifier, p, dimensionEntry.getKey()));
				}
				// If the neighbor is now a neighbor of the new peer
				if (neighbor.getZone().touch(zone, dimensionEntry.getKey())) {
					// Update new peer neighbors
					this.addNeighbor(peerEntry.getKey(), neighbor, dimensionEntry.getKey());
					// Update neighbor neighbors
					PAFuture.waitFor(neighbor.addNeighbor(this.ownerID, this.owner, dimensionEntry.getKey()));
				}
			}
		}

		// Make the new peer and the joined peer neighbors
		this.addNeighbor(pIdentifier, p, dimension);
		p.addNeighbor(this.ownerID, this.owner, dimension);
	}

	public Peer getPeerClosestTo(Zone zone, Query query) {
		Peer closest = null;
		Key key = query.getKey();
		int minDistance = Integer.MAX_VALUE;
		for (int dimension = 0 ; dimension < Constants.NB_DIMENSIONS ; dimension++) {
			if (!zone.containsOnDimension(key, dimension)) {
				for (Entry<LongWrapper, Peer> peerEntry : this.neighbors.get(dimension).entrySet()) {
					if (!query.getTraversedPeerIDs().contains(peerEntry.getKey())) {
						Bound b = peerEntry.getValue().getManagedBoundOnDimension(dimension);
						int distance = Math.min(Math.abs(b.min - key.get(dimension)),
								Math.abs(b.max - key.get(dimension)));
						if (distance < minDistance) { 
							minDistance = distance;
							closest = peerEntry.getValue();
						}
					}
				}
				return closest;
			}
		}
		// Should never happen
		System.err.println("No peer found closest to the searched value.");
		return zone.getOwner();
	}
	
	public List<Peer> getNeighbors() {
		List<Peer> peers = new LinkedList<Peer>();
		for (Entry<Integer, Map<LongWrapper, Peer>> entry: this.neighbors.entrySet()) {
			for (Entry<LongWrapper, Peer> entry2: entry.getValue().entrySet()) {
				peers.add(entry2.getValue());
			}
		}
		return peers;
	}

	public Map<Integer, Map<LongWrapper, Peer>> getNeighborsCopy() {
		Map<Integer, Map<LongWrapper, Peer>> neighborsCopy = new HashMap<Integer, Map<LongWrapper, Peer>>();
		for (Entry<Integer, Map<LongWrapper, Peer>> entry: this.neighbors.entrySet()) {
			neighborsCopy.put(entry.getKey(), new HashMap<LongWrapper, Peer>(entry.getValue()));
		}
		return neighborsCopy;
	}

	public Map<LongWrapper, Peer> getNeighborsCopyOnDimension(int dimension) {
		Map<LongWrapper, Peer> neighborsCopyOnDimension = new HashMap<LongWrapper, Peer>();
		if (this.neighbors.get(dimension) != null) {
			neighborsCopyOnDimension.putAll(this.neighbors.get(dimension));
		}
		return neighborsCopyOnDimension;
	}

	public boolean isNeighbor(Peer p) {
		if (this.neighbors.containsValue(p)) {
			return true;
		}
		return false;
	}

	public boolean isNeighborOnDimension(Peer p, int dimension) {
		if (this.neighbors.get(dimension).containsKey(p.getIdentifier())) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		Zone zone;
		StringBuilder b = new StringBuilder();
		for (Entry<Integer, Map<LongWrapper, Peer>> entry : this.neighbors.entrySet()) {
			b.append("dim" + entry.getKey());
			for (Entry<LongWrapper, Peer> neighbor : entry.getValue().entrySet()) {
				zone = neighbor.getValue().getZone();
				PAFuture.waitFor(zone);
				b.append(" " + zone.toString());
			}
			b.append(" - ");
		}
		if (b.length() > 3) {
			b.replace(b.length() - 3, b.length(), "");
		}
		return b.toString();
	}


}
