package can.policy;

import java.util.ArrayList;
import java.util.List;

import can.architecture.Peer;
import can.runtime.Constants;

public class EqualSizeNetworkBuilder extends NetworkBuilder {
	
	private static final long serialVersionUID = 1L;
	
	private List<Peer> peerSubsetToJoin;
	
	public EqualSizeNetworkBuilder() {
		this.peerSubsetToJoin = new ArrayList<Peer>();
	}

	@Override
	public Peer getPeerToJoin() {
		if (this.peerSubsetToJoin.isEmpty()) {
			copyAllPeerstoPeerSubset();
			this.splitDimension = (this.splitDimension + 1) % Constants.NB_DIMENSIONS;
		}
		Peer p = this.peerSubsetToJoin.remove(0);
		return p;
	}
	
	private void copyAllPeerstoPeerSubset() {
		for (Peer p: this.peers) {
			this.peerSubsetToJoin.add(p);
		}
	}

}
