package can.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import can.architecture.Peer;
import can.runtime.Constants;

public abstract class NetworkBuilder implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected List<Peer> peers;
	protected int splitDimension;
	
	private Peer firstPeer;
	private Peer lastPeer;
	
	protected NetworkBuilder() {
		this.peers = new ArrayList<Peer>();
		this.splitDimension = 0;
	}
	
	public List<Peer> getPeers() {
		return this.peers;
	}
	
	public void add(Peer p) {
		this.peers.add(p);
		if (this.firstPeer == null) {
			this.firstPeer = p;
		}
		this.lastPeer = p;
	}
	
	public int getSplitDimension() {
		return this.splitDimension;
	}
	
	public Peer getFirstPeer() {
		return this.firstPeer;
	}
	
	public Peer getLastPeer() {
		return this.lastPeer;
	}
	
	public Peer getRandomPeer() {
		int index = Constants.random.nextInt(this.peers.size());
		return this.peers.get(index);
	}
	
	@Override 
	public String toString() {
		StringBuilder b = new StringBuilder("NetworkBuilder:\n");
		b.append("Printing network.\n");
		for (Peer peer : peers) {
			b.append(peer.asynchronousToString() + "\n");
		}
		b.append("Network printed.");
		return b.toString();
	}
	
	public abstract Peer getPeerToJoin();

	public int getNumberOfPeers() {
		return this.peers.size();
	}

	public Peer getPeerWithId(long i) {
		return this.peers.get((int) i);
	}

}
