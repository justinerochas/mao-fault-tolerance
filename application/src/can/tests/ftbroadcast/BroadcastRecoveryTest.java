package can.tests.ftbroadcast;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.junit.Test;
import org.objectweb.proactive.ActiveObjectCreationException;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.core.ProActiveException;
import org.objectweb.proactive.core.node.Node;
import org.objectweb.proactive.core.node.NodeException;
import org.objectweb.proactive.core.process.AbstractExternalProcess;
import org.objectweb.proactive.core.process.JVMProcessImpl;
import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;
import org.objectweb.proactive.extensions.gcmdeployment.PAGCMDeployment;
import org.objectweb.proactive.gcmdeployment.GCMApplication;
import org.objectweb.proactive.gcmdeployment.GCMVirtualNode;

import can.architecture.Peer;
import can.runtime.Constants;
import can.runtime.Tracker;
import can.runtime.Utils;

/**
 * The external coordinator is a non fault tolerant active object.
 */

public class BroadcastRecoveryTest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = Logger.getLogger(BroadcastRecoveryTest.class.getName());
	private static final String logTag = "[" + BroadcastRecoveryTest.class.getSimpleName() + "] ";

	private List<Node> nodes;

	private boolean nodeAlreadyKilled;
	
	private boolean finalTestsPassed;
	
	public BroadcastRecoveryTest() {
		this.nodeAlreadyKilled = false;
		this.finalTestsPassed = false;
	}

	@Test
	public void broadcastRecoveryTest() throws Exception {
		BroadcastRecoveryTest ec = PAActiveObject.newActive(BroadcastRecoveryTest.class, new Object[] {});
		ec.execute();
		// wait for test execution for 4min and check for successful recovery
		Thread.sleep(240000);
		assertTrue(ec.getFinalTestPassed().getBooleanValue());
	}

	public void execute() {
		// Spawns the fault tolerance server
		JVMProcessImpl server = new JVMProcessImpl(
				new AbstractExternalProcess.StandardOutputMessageLogger());
		server.setClassname(Constants.FT_SERVER_CLASSNAME);
		server.setParameters(Arrays.asList(Constants.FT_PROTOCOL_PROPERTY, Constants.FT_PROTOCOL));
		try {
			server.startProcess();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
		Utils.sleep(5000);

		// Deployment node creation
		GCMApplication gcma = null; 
		try {
			gcma = PAGCMDeployment.loadApplicationDescriptor(
					Tracker.class.getResource(Constants.GCMA_LOCATION));
		} 
		catch (ProActiveException e1) {
			e1.printStackTrace();
		}
		gcma.startDeployment();
		gcma.waitReady();
		GCMVirtualNode vnm =  gcma.getVirtualNode(Constants.PEER_VN_NAME);
		nodes = new ArrayList<Node>();
		for (int i = 0 ; i < Constants.NB_PEERS ; i++) {
			nodes.add(vnm.getANode());
		}
		
		// CAN creation
		Tracker t = null;
		try {
			t = PAActiveObject.newActive(Tracker.class, new Object[] {nodes}, vnm.getANode());
		} catch (ActiveObjectCreationException | NodeException e) {
			e.printStackTrace();
		}
		t.createNetwork();
		t.addAllData((BroadcastRecoveryTest) PAActiveObject.getStubOnThis());
	}

	public void kill() {
		if (!this.nodeAlreadyKilled) {
			log.info(logTag + "Killing a node.");
			Utils.sleep(1000);
			// !!!! if a node where no active object lies is killed, then no crash, 
			// no recovery ==> check GCMD == nb peers + tracker
			Random rand = new Random();
			Node killedNode = nodes.get(rand.nextInt(nodes.size()));
			try {
				killedNode.killAllActiveObjects();
				log.info(logTag + "Node " + killedNode + " killed.");
				this.nodeAlreadyKilled = true;
			} 
			catch (NodeException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void checkCorrectBroadcastReexecution(List<Peer> peers) {
		log.info(logTag + "Trying to check on tests.");
		if (this.nodeAlreadyKilled) {
			log.info(logTag + "Checking on tests.");
			this.finalTestsPassed = true;
			for (Peer peer: peers) {
				if (peer.getBroadcastReceptionChecker() != 1) {
					this.finalTestsPassed = false;
				}
				if (peer.getBroadcastAlreadyReceived()) {
					this.finalTestsPassed = false;
				}
			}
		}
		log.info(logTag + "Checking on tests finished.");
	}
	
	public BooleanWrapper getFinalTestPassed() {
		return new BooleanWrapper(this.finalTestsPassed);
	}

}
