package can.tests.structure;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.objectweb.proactive.core.util.wrapper.LongWrapper;

import can.architecture.Bound;
import can.architecture.Peer;
import can.runtime.Constants;

/**
 * This test relates to the verification of the network shape:
 * - Ensuring that neighbors are connected
 * - Ensuring that the managed zones form the entire data space.
 */
public class JoinTest extends TestAbstract {

	@Override
	public void checkNeighborsWhenAdding(Peer p, Peer joinP, 
			Map<Integer, Map<LongWrapper, Peer>> joinPNeighbors) {
		// Check that when a peer is added, all the neighbors of the joined 
		// peer either still belong to the joined peer or the the new peer 
		// after adding.
		for (Entry<Integer, Map<LongWrapper, Peer>> entry: joinPNeighbors.entrySet()) {
			for (Entry<LongWrapper, Peer> testedNeighbor: entry.getValue().entrySet()) {
				assertTrue(
						p.getRouteManager().isNeighborOnDimension(
								testedNeighbor.getValue(), entry.getKey()) ||
						joinP.getRouteManager().isNeighborOnDimension(
								testedNeighbor.getValue(), entry.getKey()));
			}
		}
	}

	@Override
	public void checkSubsequentProperties(int dimension, List<Peer> peers) {
		System.out.println("  - No neighbor was disconnected during join operations");
		// Check that if a peer has a neighbor, then the neighbor has the peer
		// as a neighbor too
		Map<Integer, Map<LongWrapper, Peer>> pNeighbors;
		Map<Integer, Map<LongWrapper, Peer>> neighborNeighbors;
		for (Peer p: peers) {
			pNeighbors = p.getRouteManager().getNeighborsCopy();
			for (Entry<Integer, Map<LongWrapper, Peer>> neighbors: pNeighbors.entrySet()) {
				for (Entry<LongWrapper, Peer> neighbor: neighbors.getValue().entrySet()) {
					neighborNeighbors = neighbor.getValue().getRouteManager().getNeighborsCopy();
					assertTrue(neighborNeighbors.get(neighbors.getKey()).containsKey(p.getIdentifier()));
				}
			}
		}
		System.out.println("  - All peer neighbors have the peer as neighbor as well");
		// Check that the sum of peer zone interval is equal to the data space
		// interval for all dimensions (i.e. no hole in the data space)
		BigInteger dataSpaceVolume = new BigInteger("1");
		for (int i = 0 ; i < dimension ; i++) {
			dataSpaceVolume = dataSpaceVolume.multiply(new BigInteger(
					Integer.toString(
							Constants.CAN_BOUND_MAX - Constants.CAN_BOUND_MIN)));
		}		
		BigInteger peersVolume = computePeersVolume(dimension, peers);
		assertEquals(dataSpaceVolume, peersVolume);
		System.out.println("  - Sum of peer zone volumes equals to the data space volume");
	}

	private BigInteger computePeersVolume(int dimension, List<Peer> peers) {
		int zone;
		BigInteger peerVolume;
		BigInteger peersVolume = new BigInteger("0");
		for (Peer peer: peers) {
			peerVolume = new BigInteger("1");
			for (int i = 0 ; i < dimension ; i++) {
				Bound b = peer.getZone().getBoundOnDimension(i);
				zone = b.max - b.min;
				peerVolume = peerVolume.multiply(
						new BigInteger(Integer.toString(zone)));
			}
			peersVolume = peersVolume.add(peerVolume);
		}
		return peersVolume;
	}

}
