package can.tests.structure;

public class TestConstants {
	
	// Note: without active objects, all the tests pass up to dimension 100 
	// with 1000 peers
	public static final int MAX_DIMENSION = 4;
	
	public static final int MAX_PEERS = 40;
	
	public static final int MAX_ADDED_VALUES = 400;
}
