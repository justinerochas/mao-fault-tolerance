package can.tests.structure;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Test;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.api.PAFuture;
import org.objectweb.proactive.core.util.wrapper.LongWrapper;

import can.architecture.Peer;
import can.runtime.Constants;
import can.runtime.Tracker;

/**
 * Abstract test implemented using the Template design pattern.
 */
public abstract class TestAbstract {
	
	@Test
	public void test() throws Exception {

		Peer p;
		Peer joinP;
		List<Peer> peers;
		int splitDimension;
		Random r = new Random();
		Map<Integer, Map<LongWrapper, Peer>> joinPNeighbors;

		System.out.println("------------------------------------------------");
		System.out.println("Running test: " + this.getClass().getSimpleName());

		for (int dimension = 2 ; dimension <= TestConstants.MAX_DIMENSION ; 
				dimension++) {

			Constants.NB_DIMENSIONS = dimension;
			
			Tracker t = PAActiveObject.newActive(Tracker.class, new Object[] {});
			
			peers = new LinkedList<Peer>();
			p = PAActiveObject.newActive(
					Peer.class, new Object[] {new LongWrapper(0), t});	
			splitDimension = 0;
			PAFuture.waitFor(p.join(null, splitDimension));
			peers.add(p);

			System.out.println("In dimension " + dimension + ", verified properties are:");

			for (int pID = 1 ; pID <= TestConstants.MAX_PEERS ; pID++) {

				p = PAActiveObject.newActive(
						Peer.class, new Object[] {new LongWrapper(pID), t});
				splitDimension = (splitDimension + 1) % dimension;
				joinP = peers.get(r.nextInt(peers.size()));
				joinPNeighbors = joinP.getRouteManager().getNeighborsCopy();
				PAFuture.waitFor(p.join(joinP, splitDimension));
				peers.add(p);

				checkNeighborsWhenAdding(p, joinP, joinPNeighbors);
			}

			checkSubsequentProperties(dimension, peers);
		}
        
		System.out.println("------------------------------------------------");
	}

	public abstract void checkNeighborsWhenAdding(Peer p, Peer joinP, 
			Map<Integer, Map<LongWrapper, Peer>> joinPNeighbors);

	public abstract void checkSubsequentProperties(int dimension, 
			List<Peer> peers);

}
