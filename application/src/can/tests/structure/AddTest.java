package can.tests.structure;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.objectweb.proactive.api.PAFuture;
import org.objectweb.proactive.core.util.wrapper.LongWrapper;

import can.architecture.Peer;
import can.data.BalancedDataBuilder;
import can.data.Key;
import can.data.Value;
import can.messages.AddQuery;
import can.messages.AddResponse;

/**
 * Test the add operation. First a network is created and then a value which 
 * corresponds to the bottom left corner is added, we check that the first 
 * peer has this value. Then a lot of values are added and we check that for 
 * each peer, the value it stores is in the managed bounds. Finally, we check 
 * if the number of checked values corresponds to the number of added values.
 */
public class AddTest extends TestAbstract {

	@Override
	public void checkNeighborsWhenAdding(Peer p, Peer joinP, 
			Map<Integer, Map<LongWrapper, Peer>> joinPNeighbors) {
		// Left empty
	}
	
	@Override
	public void checkSubsequentProperties(int dimension, List<Peer> peers) {
		Random r = new Random();

		checkSmallestValueInsert(dimension, peers, r);

		insertValues(dimension, peers, r);

		int nbValues = checkValuesOwner(dimension, peers);

		// Check that the total number of values in the network is the number 
		// of add values 
		assertEquals(TestConstants.MAX_ADDED_VALUES, nbValues);	
		System.out.println("  - The number of values present in the network is the number of added values");
	}
	
	private void checkSmallestValueInsert(int dimension, List<Peer> peers, 
			Random r) {
		// Add smallest value in the network and check if the peer that should
		// have it has it.
		Peer departurePeer = peers.get(r.nextInt(TestConstants.MAX_PEERS));
		int[] smallestValue = new int[dimension];
		for (int i = 0 ; i < dimension ; i++) {
			smallestValue[i] = 0;
		}
		PAFuture.waitFor(departurePeer.add(new AddQuery(new Key(smallestValue), new Value())));
		assertEquals(peers.get(0).getDataManager().getStoredValuesSize(), 1);
		for (int i = 1 ; i < TestConstants.MAX_PEERS ; i++) {
			assertEquals(0, peers.get(i).getDataManager().getStoredValuesSize());
		}
		System.out.println("  - Add started from peer: " + departurePeer.getIdentifier() 
				+ " and successfully reached initial peer.");
	}

	private void insertValues(int dimension, List<Peer> peers, Random r) {
		List<Key> keysList = BalancedDataBuilder.buildKeys();
		List<Value> valuesList = BalancedDataBuilder.buildValues();
		// Add key-value pairs in the network
		Peer departurePeer;
		for (int i = 1 ; i < TestConstants.MAX_ADDED_VALUES ; i++) {
			departurePeer = peers.get(r.nextInt(TestConstants.MAX_PEERS));
			AddResponse response = departurePeer.add(new AddQuery(
					keysList.get(i), valuesList.get(i)));
			PAFuture.waitFor(response);
		}
	}
	
	private int checkValuesOwner(int dimension, List<Peer> peers) {
		// Check that each managed key belongs to the zone bounds of the peer
		int nbValues = 0;
		for (Peer checkedPeer: peers) {
			Map<Key, Value> values = checkedPeer.getDataManager().getStoredValues();
			for (Entry<Key, Value> entry: values.entrySet()) {
				nbValues++;
				for (int i = 0 ; i < dimension ; i++) {
					assertTrue(entry.getKey().get(i) >= 
							checkedPeer.getZone().getBoundOnDimension(i).min
							&& entry.getKey().get(i) < 
							checkedPeer.getZone().getBoundOnDimension(i).max);
				}
			}
		}
		System.out.println("  - All added values are hosted on the peer that should manage them");
		return nbValues;
	}

}
