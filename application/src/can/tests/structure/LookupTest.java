package can.tests.structure;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.objectweb.proactive.api.PAFuture;
import org.objectweb.proactive.core.util.wrapper.LongWrapper;

import can.architecture.Peer;
import can.data.EmptyValue;
import can.data.Key;
import can.data.Value;
import can.messages.AddQuery;
import can.messages.AddResponse;
import can.messages.LookupQuery;
import can.messages.LookupResponse;
import can.runtime.Constants;

/**
 * Test the lookup operation. First a network is created and then a value which
 * corresponds to the bottom left corner is added, we check that the first 
 * peer has this value. Then a lot of values are added and we check that for 
 * each peer, the value it stores is in the managed bounds. Finally, we check 
 * if the number of checked values corresponds to the number of added values.
 */
public class LookupTest extends TestAbstract {

	@Override
	public void checkNeighborsWhenAdding(Peer p, Peer joinP, Map<Integer, 
			Map<LongWrapper, Peer>> joinPNeighbors) {
		// Left empty	
	}
	@Override
	public void checkSubsequentProperties(int dimension, List<Peer> peers) {
		Random r = new Random();
		
		checkRetrieveNotAddedData(dimension, peers, r);

		checkRetrieveSmallestValue(dimension, peers, r);

		checkRetrieveAddedValues(dimension, peers, r); 
	}
	
	private void checkRetrieveNotAddedData(int dimension, List<Peer> peers, 
			Random r) {
		LookupResponse lookupResponse;
		// Check if not added key cannot be retrieved
		Peer departurePeer = peers.get(r.nextInt(TestConstants.MAX_PEERS));
		int[] randomPoint = new int[dimension];
		for (int j = 0 ; j < dimension ; j++) {
			randomPoint[j] = r.nextInt(Constants.CAN_BOUND_MAX);
		}
		lookupResponse = departurePeer.lookup(new LookupQuery(new Key(randomPoint)));
		PAFuture.waitFor(lookupResponse);
		assertTrue(lookupResponse.getLookupValue() instanceof EmptyValue);
		System.out.println("  - A not added point is not retrieved");
	}
	
	private void checkRetrieveSmallestValue(int dimension, List<Peer> peers,
			Random r) {
		Peer departurePeer;
		LookupResponse lookupResponse;
		// Check if smallest added value can be retrieved from peer 0
		Key k ;
		int[] smallestValue = new int[dimension];
		for (int j = 0 ; j < dimension ; j++) {
			smallestValue[j] = 0;
		}
		k = new Key(smallestValue);
		departurePeer = peers.get(r.nextInt(TestConstants.MAX_PEERS));
		AddResponse addResponse = departurePeer.add(new AddQuery(k, new Value()));
		PAFuture.waitFor(addResponse);
		departurePeer = peers.get(r.nextInt(TestConstants.MAX_PEERS));
		lookupResponse = departurePeer.lookup(new LookupQuery(k));
		PAFuture.waitFor(lookupResponse);
		assertFalse(lookupResponse.getLookupValue() instanceof EmptyValue);
		assertEquals(lookupResponse.getTraversedPeerIDs().get(0), new LongWrapper(0));
		System.out.println("  - The smallest point is retrieved on peer#0");
	}
	
	private void checkRetrieveAddedValues(int dimension, List<Peer> peers,
			Random r) {
		Key k;
		Peer departurePeer;
		LookupResponse lookupResponse;
		// Add keys in the network and check if they can be retrieved
		List<Key> addedPoints = new LinkedList<Key>();
		int[] dataPoint = null;
		for (int i = 0 ; i < TestConstants.MAX_ADDED_VALUES ; i++) {
			dataPoint = new int[dimension];
			for (int j = 0 ; j < dimension ; j++) {
				dataPoint[j] = r.nextInt(Constants.CAN_BOUND_MAX);
			}
			departurePeer = peers.get(r.nextInt(TestConstants.MAX_PEERS));
			k = new Key(dataPoint);
			AddResponse addResponse = departurePeer.add(new AddQuery(k, new Value()));
			PAFuture.waitFor(addResponse);
			addedPoints.add(k);
		}
		for (int i = 0 ; i < TestConstants.MAX_ADDED_VALUES ; i++) {
			departurePeer = peers.get(r.nextInt(TestConstants.MAX_PEERS));
			lookupResponse = departurePeer.lookup(new LookupQuery(
					addedPoints.get(i)));
			PAFuture.waitFor(lookupResponse);
			assertFalse(lookupResponse.getLookupValue() instanceof EmptyValue);
			LongWrapper lookedForId = lookupResponse.getTraversedPeerIDs().
					get(0);
			Peer rightPeerWithId = null;
			for (Peer peerWithId: peers) {
				if (peerWithId.getIdentifier().equals(lookedForId)) {
					rightPeerWithId = peerWithId;
				}
			}
			assertTrue(rightPeerWithId.manages(addedPoints.get(i)).
					getBooleanValue());
		}
		System.out.println("  - All added points can be retrieved");
	}
	
}
