package can.data;

import java.io.Serializable;
import java.util.ArrayList;

import can.runtime.Constants;

public class Key implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final ArrayList<Integer> dimensionValues;
	
	public Key(int... values) {
		this.dimensionValues = new ArrayList<Integer>(Constants.NB_DIMENSIONS);
		for (int i = 0 ; i < Constants.NB_DIMENSIONS ; i++) {
			this.dimensionValues.add(values[i]);
		}
	}
	
	public int get(int index) {
		return this.dimensionValues.get(index);
	}
	
	@Override
	public boolean equals(Object o) {
		boolean equals = true;
		if (o instanceof Key) {
			for (int i = 0 ; i < this.dimensionValues.size() ; i++) {
				if (!this.dimensionValues.get(i).equals(((Key) o).dimensionValues.get(i))) {
					return false;
				}
			}
		}
		else {
			equals = false;
		}
		return equals;
	}
	
	@Override
	public int hashCode() {
		int hashSum = 0;
		for (int i: this.dimensionValues) {
			hashSum += i;
		}
		return hashSum;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("Key [");
		for (int i: this.dimensionValues) {
			b.append(" " + i + " ");
		}
		b.append("]");
		return b.toString();
	}
}
