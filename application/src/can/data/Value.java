package can.data;

import java.io.Serializable;

import org.objectweb.proactive.core.util.wrapper.IntWrapper;

public class Value implements Serializable {
	
	private int internalValue;

	private static final long serialVersionUID = 1L;

	public Value() {
		
	}
	
	public Value(int i) {
		this.internalValue = i;
	}
	
	public IntWrapper getInternalValue() {
		return new IntWrapper(this.internalValue);
	}
	
	public void addOne() {
		this.internalValue++;
	}
}
