package can.data;

import java.util.ArrayList;
import java.util.List;

import can.runtime.Constants;

public class BalancedDataBuilder {
	
	// TODO: Beware the buildKeys procedure is not generic for any dimensions, 
	// it is only balanced for dimension 2
	public static List<Key> buildKeys() {
		int dataRange = Constants.CAN_BOUND_MAX - Constants.CAN_BOUND_MIN;
		List<Key> keysList = new ArrayList<Key>(dataRange * dataRange);
		int[] keyPoints;
		Key key;
		for (int i = Constants.CAN_BOUND_MIN ; i < Constants.CAN_BOUND_MAX ; i++) {
			for (int j = Constants.CAN_BOUND_MIN ; j < Constants.CAN_BOUND_MAX ; j++) {
				keyPoints = new int[Constants.NB_DIMENSIONS];
				keyPoints[0] = i;
				keyPoints[1] = j;
				key = new Key(keyPoints);
				keysList.add(key);
			}
		}
		return keysList;
	}
	
	public static List<Value> buildValues() {
		int dataRange = Constants.CAN_BOUND_MAX - Constants.CAN_BOUND_MIN;
		int nbValues = dataRange * dataRange;
		List<Value> valuesList = new ArrayList<Value>(nbValues);
		for (int i = 0 ; i < nbValues ; i++) {
			valuesList.add(new Value(i));
		}
		return valuesList;
	}
}
