package can.messages;

import org.objectweb.proactive.core.util.wrapper.LongWrapper;

public class AddResponse extends Message {

	private static final long serialVersionUID = 1L;
	
	/**
	 * No arg constructor needed for ProActive futures 
	 */
	public AddResponse() {
		
	}
	
	public AddResponse(LongWrapper managerId) {
		this.addtraversedPeerID(managerId);
	}
	
}
