package can.messages;

import org.objectweb.proactive.core.util.wrapper.LongWrapper;

import can.data.Value;

public class LookupResponse extends Message {

	private static final long serialVersionUID = 1L;

	private Value lookupValue;
		
	/**
	 * No arg constructor needed for ProActive futures 
	 */
	public LookupResponse() {
		
	}
	
	public LookupResponse(Value lookupValue, LongWrapper managerId) {
		this.lookupValue = lookupValue;
		this.addtraversedPeerID(managerId);
	}
	
	public Value getLookupValue() {
		return this.lookupValue;
	}

}
