package can.messages;

import can.data.Key;
import can.data.Value;

public class AddQuery extends Query {
	
	private static final long serialVersionUID = 1L;
	
	private final Value value;
	
	public AddQuery(Key key, Value value) {
		super(key);
		this.value = value;
	}
	
	public Value getValue() {
		return this.value;
	}
}
