package can.messages;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.objectweb.proactive.core.util.wrapper.LongWrapper;

public abstract class Message implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<LongWrapper> traversedPeerIDs;
	
	protected Message() {
		this.traversedPeerIDs = new LinkedList<LongWrapper>();
	}
	
	public void addtraversedPeerID(LongWrapper peerID) {
		this.traversedPeerIDs.add(peerID);
	}
	
	public List<LongWrapper> getTraversedPeerIDs() {
		return this.traversedPeerIDs;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder(this.getClass().getSimpleName() + " [traversedPeersID=<");
		for (LongWrapper peerID: this.traversedPeerIDs) {
			b.append(peerID.toString() + ",");
		}
		b.replace(b.length() - 1, b.length(), "");
		b.append(">]");
		return b.toString();
	}

}
