package can.messages;

import can.data.Key;

public class Query extends Message {

	private static final long serialVersionUID = 1L;
	
	private final Key key;
	
	
	protected Query(Key key) {
		this.key = key;
	}
	
	public Key getKey() {
		return this.key;
	}
	
}
