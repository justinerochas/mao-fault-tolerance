package can.messages;

import can.data.Key;

public class LookupQuery extends Query {

	private static final long serialVersionUID = 1L;

	public LookupQuery(Key key) {
		super(key);
	}

}
