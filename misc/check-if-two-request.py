#!/usr/bin/env python

import sys

import inspect


filename = inspect.getfile(inspect.currentframe())
print 'Running', filename

request_names = {}
seen_requests = {}
nb_requests = 0

toread = open('request-id-failure.txt', 'r')
print 'Failure'
for line in toread:
	if '**** serving request with id:' in line:
		nb_requests = nb_requests + 1 
		words = line.split(' ')
		request_id = words[-1]
		if request_id in seen_requests.keys() :
			if seen_requests[request_id] > 3 :
				print 'Request ' + request_id + ' ' + str(request_names[request_id]) + ' already appeared ' + str(seen_requests[request_id]) + ' times'
			seen_requests[request_id] = seen_requests[request_id] + 1
		else :
			request_names[request_id] = words[-2]
			seen_requests[request_id] = 1

print  'NB requests: ' + str(nb_requests)


toread = open('request-id-successful.txt', 'r')
request_names = {}
seen_requests = {}
nb_requests = 0
print 'Success'
for line in toread:
        if '**** serving request with id:' in line:
                nb_requests = nb_requests + 1
                words = line.split(' ')
                request_id = words[-1]
                if request_id in seen_requests.keys() :
                        if seen_requests[request_id] > 3 :
                        	print 'Request ' + request_id + ' ' + str(request_names[request_id]) + ' already appeared ' + str(seen_requests[request_id]) + ' times'
                        seen_requests[request_id] = seen_requests[request_id] + 1
                else :
			request_names[request_id] = words[-2]
                        seen_requests[request_id] = 1

print  'NB requests: ' + str(nb_requests)
