#!/usr/bin/env python

import os
import sys
import config
import inspect

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename

wd = os.getcwd()
os.chdir('../../..')

if not len(sys.argv) == 3 :
	print config.logger_name, 'Usage: python filename ', config.upload_options1, config.upload_options2
	sys.exit(1)
	
target_dir = ''
if sys.argv[1] == config.upload_options1[0]:
	print config.logger_name, 'Target is all.'
	target_dir = 'mao-fault-tolerance'
elif sys.argv[1] == config.upload_options1[1]:
	print config.logger_name, 'Target is ft.'
	target_dir = 'mao-fault-tolerance/scale-proactive/src/Core/org/objectweb/proactive/core/body/ft'
elif sys.argv[1] == config.upload_options1[2]:
	print config.logger_name, 'Target is tests.'
	target_dir = 'mao-fault-tolerance/scale-proactive/src/Tests/functionalTests/ft'
elif sys.argv[1] == config.upload_options1[3]:
	print config.logger_name, 'Target is nbody.'
	target_dir = 'mao-fault-tolerance/scale-proactive/examples/nbody'
elif sys.argv[1] == config.upload_options1[4]: 
	print config.logger_name, 'Target is scripts.'
	target_dir = 'mao-fault-tolerance/scripts/g5k-scripts' 

if sys.argv[2] == config.upload_options2[0] :
	print config.logger_name, 'Upload is on all sites.'
	for site in config.sites:
		print config.logger_name, "Uploading on", site, "site..."
		os.system("ssh " + config.login + "@" + config.access_site + " 'rm -Rf " + site + "/" + target_dir + "'")
		os.system("scp -r " + target_dir + " " + config.login + "@" + config.access_site + ":" + site + "/")	
		print config.logger_name, "Upload on", site, "done."	
elif sys.argv[2] == config.upload_options2[1] :
	print config.logger_name, 'Upload is on main site.'
	os.system("ssh " + config.login + "@" + config.access_site + " 'rm -Rf " + config.main_site + "/" + target_dir + "'")
	os.system("scp -r " + target_dir + " " + config.login + "@" + config.access_site + ":" + config.main_site + "/")
os.chdir(wd)


print config.logger_name, 'Ending', filename
