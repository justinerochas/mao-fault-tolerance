#!/usr/bin/env python

import os
import config
import inspect

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename

java_processes = os.popen("ps aux | grep java").read()
line_java_processes = java_processes.split("\n")

for line in line_java_processes:
	#print config.logger_name, 'Considering line: ', line
	if not "org.eclipse.equinox" in line and not "python" in line and not "ide" in line and not "grep" in line:
		words = line.split()
		cleaned_words = []
		for word in words:
			if word != '':
				cleaned_words.append(word)
		#print cleaned_words
		if len(words) > 0:
			print config.logger_name, 'Killing process ', words[1]
                	os.system("kill -9 " + words[1])
	
