# Program parameters
logger_name = '[mao-fault-tolerance]'

# Grid5000
login = 'jrochas'
access_site = 'access.grid5000.fr'
main_site = 'toulouse'
sites=['grenoble', 'lille', 'lyon', 'nancy', 'reims', 'rennes', 'toulouse', 'sophia']

upload_options1 = ['all_code', 'ft_code', 'ft_tests', 'nbody', 'scripts']
upload_options2 = ['all_sites', 'main_site']
