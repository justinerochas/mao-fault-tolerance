import java.io.Serializable;
import java.util.ArrayList;

import org.objectweb.proactive.ActiveObjectCreationException;
import org.objectweb.proactive.Body;
import org.objectweb.proactive.RunActive;
import org.objectweb.proactive.annotation.multiactivity.Compatible;
import org.objectweb.proactive.annotation.multiactivity.DefineGroups;
import org.objectweb.proactive.annotation.multiactivity.DefinePriorities;
import org.objectweb.proactive.annotation.multiactivity.DefineRules;
import org.objectweb.proactive.annotation.multiactivity.DefineThreadConfig;
import org.objectweb.proactive.annotation.multiactivity.Group;
import org.objectweb.proactive.annotation.multiactivity.MemberOf;
import org.objectweb.proactive.annotation.multiactivity.PriorityHierarchy;
import org.objectweb.proactive.annotation.multiactivity.PrioritySet;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.core.node.NodeException;
import org.objectweb.proactive.core.util.wrapper.IntWrapper;
import org.objectweb.proactive.core.util.wrapper.StringWrapper;
import org.objectweb.proactive.multiactivity.MultiActiveService;


@DefineGroups({
	@Group(name="string", selfCompatible=true),
	@Group(name="integer", selfCompatible=true)
})
@DefineRules({
	@Compatible({"string", "integer"})
})
@DefinePriorities({
	@PriorityHierarchy({
		@PrioritySet({"integer"}),
		@PrioritySet({"string"})
	})
})
@DefineThreadConfig(hardLimit = true, threadPoolSize=2)
public class OverloadedMethodTest implements RunActive, Serializable {
	
	private static final long serialVersionUID = -5662746653481095505L;

	/**
	 * Starts the multi-active service for this multiactive object.
	 */
	@Override
	public void runActivity(Body body) {
		MultiActiveService service = new MultiActiveService(body);
		while (body.isActive()) {
			service.multiActiveServing();
		}
	}
	
	@MemberOf("string")
	public void doSomething(String s)  {
		System.out.println(s);
 
	}
	
	@MemberOf("integer")
	public void doSomething(int i)  {
		System.out.println(i);

	}
	
	public static void main(String[] args) throws Exception {
		try {
			OverloadedMethodTest o = PAActiveObject.newActive(OverloadedMethodTest.class, new Object[]{});


			for (int i = 0 ; i < 100 ; i++) {
				System.out.println("sending");
				o.doSomething("toto");
				o.doSomething(55);
			}

		} 
		catch (ActiveObjectCreationException | NodeException e) {
			e.printStackTrace();
		}
		
	}

}
